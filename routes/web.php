<?php
use App\Http\Resources\GithubCollection;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::any('/', 'PageController@index')->name('home');

// Route::get('/api/v1/github', function() {
//   $repos = App\Repository::gitget();
// 
//   return new GithubCollection($repos);
// });