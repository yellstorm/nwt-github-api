<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>NWT Github API</title>
  <link rel="stylesheet" href="{{ asset('css/app.css') }}">
  <script src="{{ asset('js/app.js') }}"></script>
</head>
<body class="bg-white text-grey-darker">
  <main class="w-full max-w-md mx-auto">
    <h1 class="font-normal my-6 text-center">NWT Github API</h1>
    <p class="text-grey-darker mb-6">This application can compare two different repositories on Github. Paste in the URL to two different repos in the fields below and hit the compare button to start the magic.</p>
    <form class="" action="{{ route('home')}}" method="post">
      @csrf
      <div class="flex flex-wrap -mx-3 mb-6">
        <div class="w-full md:w-1/2 px-3 mb-6 md:mb-0">
          <label class="block uppercase tracking-wide text-grey-darker text-xs font-bold mb-2" for="repo-one">
            Repo One
          </label>
          <input class="appearance-none block w-full bg-grey-lighter text-grey-darker border border-grey-dark rounded py-3 px-4 mb-3" name="repos[]" type="text" placeholder="vuejs/vue" value="vuejs/vue">
          <p class="text-red text-xs italic hidden">Please fill out this field.</p>
        </div>
        <div class="w-full md:w-1/2 px-3">
          <label class="block uppercase tracking-wide text-grey-darker text-xs font-bold mb-2" for="repo-two">
            Repo Two
          </label>
          <input class="appearance-none block w-full bg-grey-lighter text-grey-darker border border-grey-dark rounded py-3 px-4" name="repos[]" type="text" placeholder="vuejs/vue" value="facebook/react">
        </div>
      </div>
      <div class="md:flex md:items-center">
        <button class="w-full shadow bg-purple hover:bg-purple-light text-white text-xl font-bold py-4 px-4 rounded" type="submit">Compare</button>
      </div>
    </form>
  </main>
  @if (isset($repos))
    <section class="max-w-xl mx-auto my-8 border border-grey rounded-lg overflow-hidden">
      <table class="w-full" style="border-collapse: collapse;">
        <thead>
          <tr>
            <th>Repository</th>
            @foreach ($repos as $repo)
              <th>{{ $repo->name }}</th>
            @endforeach
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>Watchers</td>
            <td>{{ $repos[0]->repo->subscribers_count }}</td>
            <td>{{ $repos[1]->repo->subscribers_count }}</td>
          </tr>
          <tr>
            <td>Stars</td>
            <td>{{ $repos[0]->repo->stargazers_count }}</td>
            <td>{{ $repos[1]->repo->stargazers_count }}</td>
          </tr>
          <tr>
            <td>Forks</td>
            <td>{{ $repos[0]->repo->forks_count }}</td>
            <td>{{ $repos[1]->repo->forks_count }}</td>
          </tr>
          <tr>
            <td>Latest release</td>
            <td>{{ $repos[0]->latest->published_at }}</td>
            <td>{{ $repos[1]->latest->published_at }}</td>
          </tr>
          <tr>
            <td>Open pull requests</td>
            <td>{{ $repos[0]->open_pr }}</td>
            <td>{{ $repos[1]->open_pr }}</td>
          </tr>
        </tbody>
      </table>
    </section>
  @endif
</body>
</html>
