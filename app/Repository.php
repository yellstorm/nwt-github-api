<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use GuzzleHttp\Client;

class Repository extends Model
{
  const whitelist = [
    'name' => 'Name',
    'full_name' => 'Full name',
    'description' => 'Description',
    'language' => 'Language',
    'license' => 'License',
    'size' => 'Size',
    'html_url' => 'URL',
    'created_at' => 'Created',
    'updated_at' => 'Updated',
    'ssh_url' => 'SSH URL',
    'clone_url' => 'HTTPS URL',
    'homepage' => 'Website',
    'watchers' => 'Watchers',
    'forks' => 'Forks',
    'open_issues' => 'Issues',
    'subscribers_count' => 'Watchers',
  ];
  public function __construct($repo)
  {
    if ( $this->verifyInput($repo) ) {
      $this->fetch();
      $this->open_pr = $this->countStatus('open');
    };
  }
  
  private function verifyInput($repo)
  {
    # TODO: Validate input or throw error
    $this->name = $repo;
    
    return true;
  }
  
  public function fetch()
  {
    $client = new Client([
      'base_uri' => 'https://api.github.com',
      'timeout'  => 5,
    ]);
    $this->repo = $this->checkResponse($client->request('GET', "/repos/{$this->name}"));
    $this->latest = $this->checkResponse($client->request('GET', "/repos/{$this->name}/releases/latest"));
    $this->pulls = $this->checkResponse($client->request('GET', "/repos/{$this->name}/pulls"));
  }
  private function checkResponse($response)
  {
    # TODO: Handle all status codes.
    if ($response->getStatusCode() === 200) {
      # TODO: Strip away all none whitelisted properties.
      return json_decode($response->getBody());
    }
  }
  
  private function countStatus($status)
  {
    $result = array_filter($this->pulls, function($pull) use ($status) {
      return $pull->state === $status;
    });
    return count($result);
  }
}
