<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
      if ($request->isMethod('post')) {
        foreach ($request->repos as $repo) {
          $repos[] = new \App\Repository($repo);
        }
        return view('home', compact('repos'));
      }

      return view('home');
    }
}
